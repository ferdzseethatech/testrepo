package compdyn.testuser.model;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import compdyn.testuser.model.HibernateUser;


public class CrudRunner {

	public static void main(String[] args) {
	/* Run all the static methods (currently only 1) */
		CrudRunner.create(); 
	}




	public static void create (){
	/* Create the config object, reading from the	hibernate.cfg.xml file. */
		  Configuration config = new Configuration();
	/* Make sure all annotated classes 	are added to the configuration */
		  config.addAnnotatedClass(HibernateUser.class);
		  SessionFactory factory;
	/* Obtain the SessionFactory after calling the config() method of the AnnotationConfiguration instance. */
		  factory = config.configure().buildSessionFactory();
	/* Get a Hibernate Session */
		  Session session = factory.getCurrentSession();
		  session.beginTransaction();
	/* Create and initialize an instance 
	of a JPA annotated class */
		  HibernateUser user = new HibernateUser();
		  user.setPassword("abc4444");
	/* Have the instance touch the session and then commit the transaction */
	    
		  session.save(user);
		  session.getTransaction().commit();  
	}
	
	@SuppressWarnings("unchecked")
	public static void retrieve() {
		Configuration config = new Configuration();
		config.addAnnotatedClass(HibernateUser.class);
		SessionFactory factory;
		factory = config.configure().buildSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		java.util.List<HibernateUser> allUsers;
		Query queryResult=session.createQuery("from User");
		allUsers = queryResult.list();
		for (int i = 0; i < allUsers.size(); i++) {
			HibernateUser user = (HibernateUser) allUsers.get(i);
			System.out.println(user.getPassword());
		}
		session.getTransaction().commit();
	}
		 

}