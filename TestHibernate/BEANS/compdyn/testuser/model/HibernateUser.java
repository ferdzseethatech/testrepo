package compdyn.testuser.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


@Entity
public class HibernateUser {

  @Id
  @GeneratedValue
  private Long id;
  private String password;
  private String name;

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  public String getPassword() {
    return password;
  }
  public void setPassword(String password) { 
    this.password = password;
  }
  
  public String getName() {
	    return name;
  }
  public void setName(String name) { 
    this.name = name;
  }

  @SuppressWarnings("unchecked")
public static void main(String args[]) {
    //AnnotationConfiguration config = new AnnotationConfiguration();
	 System.out.println("at 1");
    Configuration config = new Configuration(); 
    System.out.println("at 2");
    //config.addFile("hibernate.cfg.xml");
    System.out.println("at 3");
    config.addAnnotatedClass(HibernateUser.class);
    config.configure();
    //new SchemaExport(config).create(true, true);
    
    /*SessionFactory needs to called only once in an application!*/
    SessionFactory factory = config.buildSessionFactory();
    Session session = factory.getCurrentSession();
    
    //create an instance of user - the record to be inserted as a new row into the table!
    HibernateUser user = new HibernateUser();
    user.setName("seetha2");
    user.setPassword("xyz333");
    
    session.beginTransaction();
    session.saveOrUpdate(user);
    System.out.println("user saved");

    System.out.println("transaction successful!!!");  

    java.util.List<HibernateUser> allUsers;
    Query queryResult=session.createQuery("from HibernateUser");
    allUsers = queryResult.list();
    for (int i = 0; i < allUsers.size(); i++) {
      HibernateUser user1 = (HibernateUser) allUsers.get(i);
      System.out.println(user1.getPassword());
    }

    session.getTransaction().commit();
  }
  
  
}
